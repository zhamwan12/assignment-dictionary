global find_word
extern string_length
extern string_equals

	
find_word
	xor rax, rax
	mov r8, rdi					
	mov r9, rsi					
.loop
	add r9, 8			
	mov rsi, r9 			
	mov rdi, r8	
	push r8
	push r9
	call string_equals		
	pop r9
	pop r8
	cmp rax, 1
	je .goodEnd
	mov r9, [r9-8]	
	cmp r9, 0				
	je .badEnd
	jmp .loop
.goodEnd	
	sub r9, 8			
	mov rax, r9
	ret
.badEnd
	xor rax, rax
	ret
