%define NEXTEL 0					
%macro colon 2
	%ifid %2								
		%2: dq NEXTEL			
		%define NEXTEL %2		
	%else
		%fatal "expected word"
	%endif
	%ifstr %1						
		db %1, 0					
	%else
		%fatal "expected key"
	%endif
%endmacro 
