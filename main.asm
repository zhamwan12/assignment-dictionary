%include "words.inc"
%include "lib.inc"


%define SIZE 256



extern find_word


global _start
section .data


keyExist:
	db "the key does not exist", 0
overflow:
	db "overflow: string len >255", 0
buffer:
	times SIZE db 0


section .text


_start:
	xor rax, rax
	mov rdi, buffer
	mov rsi, SIZE
	call read_word
	test rax, rax			
	jne .notOverflow	
	mov rdi, overflow
	call error
	call print_newline
	call exit
.notOverflow:
	mov rdi, rax
	mov rsi, word1
	push rdx			
	call find_word
	test rax, rax	
	pop rdx		
	jne .success
	mov rdi, keyExist
	call error
	call print_newline
	call exit
.success:			
	add rax, 8		
	add rax, rdx			
	add rax, 1			
	mov rdi, rax
	call print_string
	call print_newline
	call exit
error:
	xor rax, rax
	mov rsi, rdi
	call string_length
	mov rdx, rax
	mov rdi, 2
	mov rax, 1
	syscall
	ret
