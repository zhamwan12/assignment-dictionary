ASM=nasm
FLAGS=-f elf64
LD=ld


prog: main.o dict.o lib.o
	$(LD) -o $@ $^

main.o: main.asm dict.o lib.o *.inc
	$(ASM) $(FLAGS) -o $@ $<

dict.o: diсt.asm lib.o lib.inc
	$(ASM) $(FLAGS) -o $@ $<

lib.o: lib.asm
	$(ASM) $(FLAGS) -o $@ $<

clean:
	rm -f *.o 

.PHONY: clean
